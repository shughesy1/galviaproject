<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;



class HomeController extends Controller
{
    public function Home()
    {
        // $task= new Task([
        //     'content' =>'test'

        // ]);
        // $task->save();

        return view('home',[
            'tasks' => Task::all()

        ]);
    }


}
