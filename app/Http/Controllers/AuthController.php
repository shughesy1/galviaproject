<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(){

        return view('auth.register');
    }

    public function store(){

        $validated = request()->validate(

            [
                'name' => 'required|min:5|max:15',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|confirmed|min:6'
            ]
            );

           User::create(
                [
                    'name' => $validated['name'],
                    'email' => $validated['email'],
                    'password' => Hash::make($validated['password']),
                ]
                );

        return redirect()->route('home') ->with('success', 'Account created succesfully!');
    }

    public function login(){

        return view('auth.login');
    }

    public function authenticate(){



        $validated = request()->validate(

            [

                'email' => 'required|email',
                'password' => 'required|min:6'
            ]
            );

            if (auth()->attempt($validated)) {
                request()->session()->regenerate();

                return redirect()->route('home') ->with('success', 'Account created succesfully!');
            }

            return redirect()->route('login')->withErrors([
                'email' => "Invalid Email or password."
            ]);

    }

    public function logout(){

        auth()->logout();

        request()->session()->invalidate();
        request()->session()->regenerateToken();

        return redirect()->route('home') ->with('success', 'logged out succesfully!');
    }
}
