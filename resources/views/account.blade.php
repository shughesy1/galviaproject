<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{config('app.name')}}</title>
    <!-- Bootstrap CSS with Sketchy theme from Bootswatch CDN -->
    <link href="https://bootswatch.com/5/sketchy/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <!-- Font Awesome for icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1">{{config('app.name')}}</span>
            <div class="d-flex align-items-center">
                <a class="navbar-brand" href="#">
                    <!-- Place your logo image here -->
                    <img src="path-to-your-logo.png" alt="logo" style="height: 30px;">
                </a>
                <a class="ms-auto nav-link" href="#">To do web app</a>
            </div>
        </div>
    </nav>

    <div class="container mt-5">
        <div class="row justify-content-end">
            <div class="col-md-8">
                <h2 class="text-center mb-4">Account</h2>
                <form>
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="username" placeholder="Username" aria-label="Username">
                            <button class="btn btn-outline-secondary" type="button">Change</button>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <div class="input-group">
                            <input type="password" class="form-control" id="password" placeholder="Password" aria-label="Password">
                            <button class="btn btn-outline-secondary" type="button">Change</button>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <div class="input-group">
                            <input type="email" class="form-control" id="email" placeholder="Email" aria-label="Email">
                            <button class="btn btn-outline-secondary" type="button">Change</button>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="phone-number" class="form-label">Phone Number</label>
                        <div class="input-group">
                            <input type="tel" class="form-control" id="phone-number" placeholder="Phone Number" aria-label="Phone Number">
                            <button class="btn btn-outline-secondary" type="button">Change</button>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="account-code" class="form-label">Account Code</label>
                        <input type="text" class="form-control" id="account-code" placeholder="Account Code" aria-label="Account Code" disabled>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <!-- Right Sidebar -->
                <div class="mb-2">
                    <a href="#" class="list-group-item list-group-item-action bg-light">Logout</a>
                </div>
                <div class="list-group">
                    <a href="home" class="list-group-item list-group-item-action">Home</a>
                    <a href="#" class="list-group-item list-group-item-action">Friends</a>
                    <a href="about" class="list-group-item list-group-item-action">About</a>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer bg-light text-center mt-5 p-3">
        <span>Done it</span>
        <div>
            <!-- Placeholder for social media icons -->
            <!-- You can use Font Awesome icons here -->
        </div>
        <span>Copyright ©</span>
    </footer>

    <!-- Bootstrap Bundle with Popper from CDN -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</body>

</html>


