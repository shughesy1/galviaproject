<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Task Management</title>
    <link href="https://bootswatch.com/5/sketchy/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1">DoneIt</span>
            <div class="d-flex align-items-center">
                <a class="navbar-brand" href="#">
                    <img src="path-to-your-logo.png" alt="logo" style="height: 30px;">
                </a>
                <a class="ms-auto nav-link" href="#">to do web app</a>
            </div>
        </div>
    </nav>

    <div class="container mt-5">
        <div class="row">
            <!-- Left Sidebar -->
            <div class="col-md-3">
                @auth
                <h5>Welcome {{Auth::user()->name}}</h5>
                @endauth
                <p id="currentTasksCounter">current tasks: 0</p>
                <p id="completedTasksCounter">Completed tasks: 0</p>
            </div>

            <!-- Task Feed -->
            <div class="col-md-6">
                <h2 class="text-center mb-4">Task Feed</h2>
                <!-- Add Task Form -->
                <div class="mb-3">
                    <textarea class="form-control" id="taskInput" rows="3" placeholder="Add task"></textarea>
                    <button class="btn btn-outline-secondary mt-2" type="button" onclick="addTask()">Add Task</button>
                </div>
                <hr>
                <!-- Task List -->
                <div id="taskList" class="mb-3">
                    <!-- Task items will be dynamically added here -->
                </div>
            </div>

            <!-- Right Sidebar -->
            <div class="col-md-3">
                <div class="list-group">
                    @guest
                    <form action="{{route('login')}}" method="GET">
                    @csrf
                    <button class="list-group-item list-group-item-action" type="submit">Login</button>
                    @endguest
                    <a href='about' class="list-group-item list-group-item-action">About</a>
                    @auth
                    <a href="account" class="list-group-item list-group-item-action">Account</a>
                    <a href="#" class="list-group-item list-group-item-action">Friends</a>
                    <form action="{{route('logout')}}" method="POST">
                    @csrf
                    <button class="list-group-item list-group-item-action" type="submit">Logout</button>
                    @endauth
                </div>
            </div>
        </div>
    </div>

    <footer class="footer bg-light text-center mt-5 p-3">
        <div>
            <!-- Placeholder for social media icons -->
        </div>
        <span>Copyright ©</span>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

    <script>
        let currentTasksCount = 0;
        let completedTasksCount = 0;

        function addTask() {
            const taskInput = document.getElementById('taskInput');
            const taskContent = taskInput.value.trim();
            if (taskContent) {
                currentTasksCount++;
                updateTaskCounters();

                const taskList = document.getElementById('taskList');
                const taskCard = document.createElement('div');
                taskCard.classList.add('card', 'mb-3');
                const cardBody = document.createElement('div');
                cardBody.classList.add('card-body');
                cardBody.innerHTML = `
                    <div class="w-100">
                        <h5 class="card-title">${taskContent}</h5>
                        <p class="card-text">Created: ${new Date().toLocaleString()}</p>
                        <button class="btn btn-success btn-sm" onclick="completeTask(this.closest('.card'))">Completed</button>
                    </div>
                `;
                taskCard.appendChild(cardBody);
                taskList.prepend(taskCard);
                taskInput.value = ''; // Clear input field after adding task
            }
        }

        function completeTask(taskCard) {
            taskCard.remove();
            currentTasksCount--;
            completedTasksCount++;
            updateTaskCounters();
        }

        function updateTaskCounters() {
            document.getElementById('currentTasksCounter').textContent = `current tasks: ${currentTasksCount}`;
            document.getElementById('completedTasksCounter').textContent = `Completed tasks: ${completedTasksCount}`;
        }
    </script>
</body>
</html>

