<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{config('app.name')}}</title>
    <!-- Bootstrap CSS with Sketchy theme from Bootswatch CDN -->
    <link href="https://bootswatch.com/5/sketchy/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">

    <!-- Font Awesome for icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">{{config('app.name')}}</a>
            <div class="d-flex">
                <a class="nav-link" href="#">To do web app</a>
            </div>
        </div>
    </nav>

    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header text-center">
                        Login
                    </div>
                    <div class="card-body">
                        <form lass="mt-0" action="{{'login'}}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="email" class="text-dark">Email</label><br>
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="text-dark">Password</label><br>
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                            </div>
                            <div class ="form-group">
                                <label for="remember me" class="text-dark"></label><br>
                                <input type="submit" name="submit" class="btn btin-dark btn-md" value="submit">
                            </div>
                        </form>
                    </div>
                    <div class="card-footer text-muted text-center">
                        <a href="/register" class="text-dark">Register here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer bg-light text-center mt-5 p-3">
        <span>Done it</span>
        <div>
            <!-- Placeholder for social media icons -->
            <!-- You can use Font Awesome icons here -->
        </div>
        <span>Copyright ©</span>
    </footer>

    <!-- Bootstrap Bundle with Popper from CDN -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>
</body>

</html>
