<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{config('app.name')}}</title>
    <!-- Bootstrap CSS with Sketchy theme from Bootswatch CDN -->
    <link href="https://bootswatch.com/5/sketchy/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <!-- Font Awesome for icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1">{{config('app.name')}}</span>
            <div class="d-flex align-items-center">
                <a class="navbar-brand" href="#">
                    <!-- Place your logo image here -->
                    <img src="path-to-your-logo.png" alt="logo" style="height: 30px;">
                </a>
                <a class="ms-auto nav-link" href="#">To do web app</a>
            </div>
        </div>
    </nav>

    <div class="container mt-5">
        <div class="row justify-content-end">
            <div class="col-md-8">
                <h2 class="text-center">About</h2>
                <div class="bg-light p-4 rounded-3">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nunc sit amet commodo. Nulla facilisi. Phasellus vestibulum, quam tincidunt venenatis ultrices, est libero mattis ante, ac consectetur diam neque eget quam. Etiam egestas viverra lorem.</p>
                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed in dui ac ipsum feugiat ultrices. Mauris at velit ac urna venenatis faucibus. Nullam hendrerit ipsum vel ex dictum, non vehicula tortor tristique. In in nisi nec velit maximus dapibus. Fusce ac libero est. Curabitur at dolor eget nulla auctor fringilla. Aenean sed tortor aliquet, volutpat risus a, consequat lorem. Sed eu dolor id enim finibus aliquet nec eget quam. Suspendisse potenti.</p>
                </div>
            </div>
            <div class="col-md-2">
                <!-- Right Sidebar -->
                <div class="mb-2">
                    <a href="#" class="list-group-item list-group-item-action bg-light">Logout</a>
                </div>
                <div class="list-group">
                    <a href="account" class="list-group-item list-group-item-action">Account</a>
                    <a href="#" class="list-group-item list-group-item-action">Friends</a>
                    <a href="home" class="list-group-item list-group-item-action">Home</a>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer bg-light text-center mt-5 p-3">
        <span>Done it</span>
        <div>
            <!-- Placeholder for social media icons -->
            <!-- You can use Font Awesome icons here -->
        </div>
        <span>Copyright ©</span>
    </footer>

    <!-- Bootstrap Bundle with Popper from CDN -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</body>

</html>



