<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/login', [AuthController::class , 'login'])->name('login');

Route::post('/login', [AuthController::class , 'authenticate']);

Route::post('/logout', [AuthController::class , 'logout'])->name('logout');

Route::get('/register', [AuthController::class , 'register'])->name('register');

Route::post('/register', [AuthController::class , 'store']);

Route::get('/about', [AboutController::class , 'About'] );


Route::get('/account', [AccountController::class , 'Account'] );


Route::get('/home', [HomeController::class, 'home'])->name('home');


// Route to add a new task
Route::post('/add-task', [HomeController::class, 'addTask'])->name('addTask');

// Route to mark a task as completed
Route::post('/complete-task', [HomeController::class, 'completeTask'])->name('completeTask');
